import cv2
import numpy as np

def show_img(title, img):
    cv2.imshow(title, img) # * 255 and show
    cv2.waitKey(0)

# Read img into arr and show
path_rice = r"../original_imgs/rice.png"
path_rose = r"../original_imgs/rose.jpg"
img_rice = cv2.cvtColor(cv2.imread(path_rice), cv2.COLOR_BGR2GRAY)
img_rose = cv2.cvtColor(cv2.imread(path_rose), cv2.COLOR_BGR2GRAY)
show_img("Rose", img_rose)

# Cast to uint16
img_rice_uint16 = np.uint16(img_rice.copy())
print("Size rice: {}".format(img_rice_uint16.shape))
img_rose_uint16 = np.uint16(img_rose.copy())
print("Size rose: {}".format(img_rose_uint16.shape))

# Resize rose
show_img("Before resize", img_rose_uint16/255)
resized_img_rose_uint16 = cv2.resize(img_rose_uint16,(256, 256))
show_img("After resize", resized_img_rose_uint16/255)

# Subtract 2 img with each others
show_img("Rose - Rice", (resized_img_rose_uint16 - img_rice_uint16)/255)
show_img("Rice - Rose", (img_rice_uint16 - resized_img_rose_uint16)/255)

cv2.destroyAllWindows()