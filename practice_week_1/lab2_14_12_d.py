import cv2
import numpy as np

def show_img(title, img):
    cv2.imshow(title, img) # * 255 and show
    cv2.waitKey(0)

# Read img into arr and show
path_rice = r"../original_imgs/rice.png"
path_rose = r"../original_imgs/rose.jpg"
img_rice = cv2.cvtColor(cv2.imread(path_rice), cv2.COLOR_BGR2GRAY)
img_rose = cv2.cvtColor(cv2.imread(path_rose), cv2.COLOR_BGR2GRAY)
show_img("Rose", img_rose)

# Cast to uint16
img_rice_uint16 = np.uint16(img_rice.copy())
print("Size rice: {}".format(img_rice_uint16.shape))
img_rose_uint16 = np.uint16(img_rose.copy())
print("Size rose: {}".format(img_rose_uint16.shape))

# Calc imcomplement
img_rose_imcomplement = 255 - img_rose
show_img("Rose imcomplement", img_rose_imcomplement)

cv2.destroyAllWindows()