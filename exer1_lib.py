from exer1_my_lib import  *
from PIL import Image

# Read img into arr and show
path = r"./original_imgs/lc.jpeg"
img = cv2.imread(path)

gray2 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

thresh, bw2 = cv2.threshold(gray2, 127, 255, cv2.THRESH_BINARY)

invert_img = cv2.bitwise_not(gray2)

# Quantize
im1 = Image.open(path)
im1 = im1.quantize(4)

# Log trans, Gamma trans (cv2 don't have func, it's too short, but you need to check again)

hist_equal_img = cv2.equalizeHist(gray2)

if __name__== "__main__":
    show_img("Original img", img)
    show_img("Gray (lib)", gray2)
    show_img("BW (lib)", bw2)
    show_img("Invert (lib)", invert_img)
    show_img("Hist equal (lib)", hist_equal_img)
    im1.show()

    cv2.destroyAllWindows()