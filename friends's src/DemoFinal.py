import cv2
import numpy as np
from tkinter import *
from PIL import Image, ImageTk
import PIL

def loadRBG(lab):
    img = Image.open("data/ImgDemo/image.jpg")
    photo = ImageTk.PhotoImage(img)
    lab.configure(image=photo)
    lab.image = photo
    return photo

def convertRBGToGray(image, lab):
    image = np.array(image)
    [r, c, h] = image.shape
    image = (np.uint32(image[:, :, 0]) + np.uint32(image[:, :, 1]) + np.uint32(image[:, :, 2]))/3
    image = np.uint8(image)
    image = PIL.Image.fromarray(image)
    photo = ImageTk.PhotoImage(image)
    lab.configure(image=photo)
    lab.image = photo
    return image

def convertGrayToBinary(image, lab):
    image = np.array(image)
    [r, c] = image.shape
    for i in range(r):
        for j in range(c):
            if image[i, j] >= 125:
                image[i, j] = 255
            else:
                image[i, j] = 0
    image = PIL.Image.fromarray(image)
    photo = ImageTk.PhotoImage(image)
    lab.configure(image=photo)
    lab.image = photo
    return image

def convertBinaryToYinTable(image, lab):
    image = np.array(image)
    [r, c] = image.shape
    image = 255 - image
    image = PIL.Image.fromarray(image)
    photo = ImageTk.PhotoImage(image)
    lab.configure(image=photo)
    lab.image = photo
    return image

def inAndDeBrightness(image, lab, txt):
    image = np.array(image)
    [r, c] = image.shape
    image = image + int(txt.get())
    image = PIL.Image.fromarray(image)
    photo = ImageTk.PhotoImage(image)
    lab.configure(image=photo)
    lab.image = photo
    return image
window = Tk()
window.title('Demo')
window.geometry('710x720')

img = Image.open("data/ImgDemo/image.jpg")
photo = ImageTk.PhotoImage(img)
lab = Label(image=photo)
lab.place(x=20,y=20)

btn = Button(text='Chuyển sang ảnh xám', command=lambda :convertRBGToGray(img, lab))
btn.place(x=100,y=650)
btn1 = Button(text='Chuyển sang ảnh nhị phân', command=lambda :convertGrayToBinary(convertRBGToGray(img, lab), lab))
btn1.place(x=250,y=650)
btn2 = Button(text='Chuyển sang âm bảng', command=lambda :convertBinaryToYinTable(convertGrayToBinary(convertRBGToGray(img, lab), lab), lab))
btn2.place(x=430,y=650)

txt = Entry(window, width=18)
txt.place(x=250,y=690)
txt.insert(INSERT, 'Nhập số nguyên...')

btn3 = Button(text='Tăng giảm độ sáng', command=lambda :inAndDeBrightness(convertRBGToGray(img, lab), lab, txt))
btn3.place(x=430,y=690)

btn4 = Button(text='Chuyển sang ảnh màu', command=lambda :loadRBG(lab))
btn4.place(x=100,y=690)

window.mainloop()