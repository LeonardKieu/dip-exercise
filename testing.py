import tkinter as tk
import numpy as np
from PIL import Image, ImageTk
import exer1_lib

root = tk.Tk()

array = np.ones((40,40))*150
img =  ImageTk.PhotoImage(image=Image.fromarray(exer1_lib.gray2))

canvas = tk.Canvas(root)
canvas.pack()
canvas.create_image(20,20, anchor="nw", image=img)

root.mainloop()
