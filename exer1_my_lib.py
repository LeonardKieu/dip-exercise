import cv2
import numpy as np
from matplotlib import pyplot as plt

def calculateEuclideanDistance(i1, i2):
    return np.sum((i1 - i2) ** 2)
def calc_different_i1i2(i1, i2):
    w, h = i1.shape
    return calculateEuclideanDistance(i1, i2) / (255 ** 2 * w * h)
def show_img(title, img):
    cv2.imshow(title, img) # * 255 and show
    cv2.waitKey(0)
def convert_RGB2Gray(img):
    img = np.uint32(img.copy()) # make a deep copy and point to it
    return np.uint8((img[:, :, 0] + img[:, :, 1] + img[:, :, 2]) / 3) # cast to old datatype - uint8
def convert_Gray2BW(img, threshold = 127):
    img = img.copy()
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if(img[i,j] >= threshold):
                img[i,j] = 255
            else:
                img[i, j] = 0
    return img
def negative(img):
    return 255 - img
def quantize(img):
    img = img.copy()
    # img = (np.floor(np.float64(img) / 64) * 64).astype(np.uint8)
    img = np.uint8(img / 64) * 64 # compact expression
    return img
def log_transform(img):
    img = np.uint32(img.copy())
    return np.uint8(45 * np.log10(1 + img ** 2))
def gamma_transform(img, c = 1, g = 1.01):
    img = np.uint32(img.copy())
    img = c * np.power(img, g)
    # return np.uint8(img)
    return img
def histogram_equalization(img):
    img = np.uint32(img.copy())
    height, width = img.shape
    hist, bins = np.histogram(img.flatten(), 256, [0, 256])
    cdf = hist.cumsum()

    cdf_m = np.ma.masked_equal(cdf, 0) # don't mask any elem to apply func through the img
    cdf_m2 = 255 * cdf_m / (height * width)
    cdf_final = np.ma.filled(cdf_m2, 0).astype('uint8')

    return cdf_final[img]
