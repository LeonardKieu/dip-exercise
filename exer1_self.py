from exer1_my_lib import  *

# Read img into arr and show
path = r"./original_imgs/lc.jpeg"
img_uint8 = cv2.imread(path)
img = img_uint8
show_img("Original img", img)

# RBG to Gray
gray_uint8 = convert_RGB2Gray(img)
show_img("Gray", gray_uint8)
cv2.imwrite(r"./processed_imgs/graylc.jpeg", gray_uint8)

# Gray to BW
bw = convert_Gray2BW(gray_uint8)
show_img("BW", bw)
cv2.imwrite(r"./processed_imgs/bwlc.jpeg", bw)

# Negative
negative_img = negative(gray_uint8)
show_img("Negative", negative_img)
cv2.imwrite(r"./processed_imgs/negative_img.jpeg", negative_img)

# Quantize
quantized_img = quantize(gray_uint8)
show_img("Quantized img", quantized_img)
cv2.imwrite(r"./processed_imgs/quantized_img.jpeg", quantized_img)

# Log transform
log_transformed_img = log_transform(gray_uint8)
show_img("Log transform", log_transformed_img)
cv2.imwrite(r"./processed_imgs/log_trans_img.jpeg", log_transformed_img)

# Gamma transform
gamma_correction = [2.0, 3.0, 4.0, 4.5, 5.0]
for g in gamma_correction:
    gamma_transformed_img = gamma_transform(gray_uint8, g = g)
    show_img("Gamma transformation with c = 1, g = {}".format(g), gamma_transformed_img)
cv2.imwrite(r"./processed_imgs/gamma.jpeg", gamma_transformed_img)

# Hist equalization
hist_equal_img = histogram_equalization(gray_uint8)
show_img("Hist equal", hist_equal_img)



cv2.destroyAllWindows()